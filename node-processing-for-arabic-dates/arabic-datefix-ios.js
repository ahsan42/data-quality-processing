const StreamArray = require('stream-json/streamers/StreamArray');
const {Writable} = require('stream');
const path = require('path');
const fs = require('fs');

const fileStream = fs.createReadStream(path.join('/', 'app/temp/ios_events_with_arabic_dates_as_array.json'));
const jsonStream = StreamArray.withParser();


String.prototype.ArtoEn= function() {
  return this.replace(/[\u0660-\u0669]/g, 
    d => d.charCodeAt() - 1632)
}

const processingStream = new Writable({
    write({key, value}, encoding, callback) {
        //Save to mongo or do any other async actions
        
        var i=0;
        while(i<value.event.length){
 
                    
                value.journey_start=value.journey_start.ArtoEn();
                value.journey_end=value.journey_end.ArtoEn();
                value.event[i].created_at=value.event[i].created_at.ArtoEn();
                i++;
        }
        
        
        
        const jsonWrite = JSON.stringify(value)
        fs.writeFile('/app/temp/ios_events_fixed_dates.json', jsonWrite + "\n", { flag: "a+" }, function (err) {
            if (err) throw err;
        });

        setTimeout(() => {
            //console.log(value);
            //Next record will be read only current one is fully processed
            callback();
        }, 0);
    },
    //Don't skip this, as we need to operate with objects, not buffers
    objectMode: true
});

//Pipe the streams as follows
fileStream.pipe(jsonStream.input);
jsonStream.pipe(processingStream);

//So we're waiting for the 'finish' event when everything is done.
processingStream.on('finish', () => console.log('All done'));





