const StreamArray = require('stream-json/streamers/StreamArray');
const {Writable} = require('stream');
const path = require('path');
const fs = require('fs');

const fileStream = fs.createReadStream(path.join('/', 'app/temp/android_events_for_mMap.json'));
const jsonStream = StreamArray.withParser();

        var count = 0;
const processingStream = new Writable({
    write({key, value}, encoding, callback) {
        //Save to mongo or do any other async actions
        
        var i=0;

        while(i<value.event.length){

            if(!(value.event[i].event_attributes === undefined) && !(value.event[i].event_attributes.mMap === undefined)){

              if(!(value.event[i].event_attributes.mMap.meal_id === undefined)){
                value.event[i].event_attributes.meal_id = value.event[i].event_attributes.mMap.meal_id
              }
              
              if(!(value.event[i].event_attributes.mMap.menu_id === undefined)){
                value.event[i].event_attributes.menu_id = value.event[i].event_attributes.mMap.menu_id
              }
              
              if(!(value.event[i].event_attributes.mMap.menu_date === undefined)){
                value.event[i].event_attributes.menu_date = value.event[i].event_attributes.mMap.menu_date
              }
              
              if(!(value.event[i].event_attributes.mMap.branch_id === undefined)){
                value.event[i].event_attributes.branch_id = value.event[i].event_attributes.mMap.branch_id
              }
              
              if(!(value.event[i].event_attributes.mMap.timing_id === undefined)){
                value.event[i].event_attributes.timing_id = value.event[i].event_attributes.mMap.timing_id
              }
              
              if(!(value.event[i].event_attributes.mMap.order_id === undefined)){
                value.event[i].event_attributes.order_id = value.event[i].event_attributes.mMap.order_id
              }
              
              if(!(value.event[i].event_attributes.mMap.timing_slot === undefined)){
                value.event[i].event_attributes.timing_slot = value.event[i].event_attributes.mMap.timing_slot
              }
              
              delete value.event[i].event_attributes.mMap
              console.log(value.event[i].event_attributes);
                
            }

            i++;
        }
        
        
        count++;

        const jsonWrite = JSON.stringify(value)
        fs.writeFile('/app/temp/android_events_fixed_mMap.json', jsonWrite + "\n", { flag: "a+" }, function (err) {
            if (err) throw err;
        });
//         console.log(count);

        setTimeout(() => {
            //console.log(value);
            //Next record will be read only current one is fully processed
            callback();
        }, 0);
    },
    //Don't skip this, as we need to operate with objects, not buffers
    objectMode: true
});

//Pipe the streams as follows
fileStream.pipe(jsonStream.input);
jsonStream.pipe(processingStream);

//So we're waiting for the 'finish' event when everything is done.
processingStream.on('finish', () => console.log(count));





