const StreamArray = require('stream-json/streamers/StreamArray');
const {Writable} = require('stream');
const path = require('path');
const fs = require('fs');

const fileStream = fs.createReadStream(path.join('/', 'app/temp/ios_events_for_journey_id.json'));
const jsonStream = StreamArray.withParser();


const processingStream = new Writable({
    write({key, value}, encoding, callback) {
        //Save to mongo or do any other async actions
        
        var i=0;
        while(i<value.event.length){
 
                    
                value.event[i].journey_id=value.journey_id;
                i++;
        }
        
        
        
        const jsonWrite = JSON.stringify(value)
        fs.writeFile('/app/temp/ios_events_fixed_journey_id.json', jsonWrite + "\n", { flag: "a+" }, function (err) {
            if (err) throw err;
        });

        setTimeout(() => {
            //console.log(value);
            //Next record will be read only current one is fully processed
            callback();
        }, 0);
    },
    //Don't skip this, as we need to operate with objects, not buffers
    objectMode: true
});

//Pipe the streams as follows
fileStream.pipe(jsonStream.input);
jsonStream.pipe(processingStream);

//So we're waiting for the 'finish' event when everything is done.
processingStream.on('finish', () => console.log('All done'));





