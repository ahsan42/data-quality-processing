const StreamArray = require('stream-json/streamers/StreamArray');
const {Writable} = require('stream');
const path = require('path');
const fs = require('fs');

const fileStream = fs.createReadStream(path.join('/', 'app/temp/web_events_for_namecodescreen.json'));
const jsonStream = StreamArray.withParser();

        var count = 0;
const processingStream = new Writable({
    write({key, value}, encoding, callback) {
        //Save to mongo or do any other async actions
        
        var i=0;

        while(i<value.event.length){



              if(!(value.event[i].name === undefined)){
                value.event[i].event_name = value.event[i].name
              }
              
              if(!(value.event[i].code === undefined)){
                value.event[i].event_code = value.event[i].code
              }
              
              if(!(value.event[i].screen === undefined)){
                value.event[i].screen_name = value.event[i].screen
              }
              
              
              
              delete value.event[i].name
              delete value.event[i].code
              delete value.event[i].screen
              

                


            i++;
        }
        
        
        count++;

        const jsonWrite = JSON.stringify(value)
        fs.writeFile('/app/temp/web_events_fixed_namecodescreen.json', jsonWrite + "\n", { flag: "a+" }, function (err) {
            if (err) throw err;
        });
//         console.log(count);

        setTimeout(() => {
            //console.log(value);
            //Next record will be read only current one is fully processed
            callback();
        }, 0);
    },
    //Don't skip this, as we need to operate with objects, not buffers
    objectMode: true
});

//Pipe the streams as follows
fileStream.pipe(jsonStream.input);
jsonStream.pipe(processingStream);

//So we're waiting for the 'finish' event when everything is done.
processingStream.on('finish', () => console.log(count));





